@extends('Admin.master')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">
            <h2>داستان ها</h2>
            <a href="{{ route('stories.create') }}" class="btn btn-sm btn-primary">ارسال داستان</a>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>عنوان داستان</th>
                    <th>تعداد نظرات</th>
                    <th>مقدار بازدید</th>
                    <th>تنظیمات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($stories as $story)
                    <tr>
                        <td><a href="{{ $story->path() }}">{{ $story->title }}</a></td>
                        <td>{{ $story->commentCount }}</td>
                        <td>{{ $story->viewCount }}</td>
                        <td>
                            <form action="{{ route('stories.destroy'  , $story->id) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <a href="{{ route('stories.edit' , $story->id) }}"  class="btn btn-primary">ویرایش</a>
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div style="text-align: center">
            {!! $stories->render() !!}
        </div>
    </div>
@endsection
