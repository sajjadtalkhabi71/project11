<?php

namespace App\Http\Controllers\Admin;

use App\Story;
use App\Http\Requests\StoryRequest;
use Illuminate\Http\Request;

class StoryController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stories = Story::latest()->paginate(20);
        return view('Admin.stories.all' , compact('stories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.stories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoryRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoryRequest $request)
    {
        $imagesUrl = $this->uploadImages($request->file('images'));
        auth()->user()->story()->create(array_merge($request->all() , [ 'images' => $imagesUrl]));

        return redirect(route('stories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function show(Story $story)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function edit(Story $story)
    {
        return view('Admin.stories.edit' , compact('story'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoryRequest|Request $request
     * @param  \App\Story $story
     * @return \Illuminate\Http\Response
     */
    public function update(StoryRequest $request, Story $story)
    {
        $file = $request->file('images');
        $inputs = $request->all();

        if($file) {
            $inputs['images'] = $this->uploadImages($request->file('images'));
        } else {
            $inputs['images'] = $story->images;
            $inputs['images']['thumb'] = $inputs['imagesThumb'];

        }

        unset($inputs['imagesThumb']);
        $story->update($inputs);

        return redirect(route('stories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function destroy(Story $story)
    {
        $story->delete();
        return redirect(route('stories.index'));
    }
}
