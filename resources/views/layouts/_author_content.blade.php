<!-- main --->
<main id="main">
    <div class="container">
        <div class="row">
            <!-- main content --->
            <div class="col-md-8">


                <h3 class="page-title mb-0 pt-5 pb-5"> <span>نوشته های</span> <a href="../author.blade.php" title="">کاربر</a> </h3>

                <!-- main body --->
                <div class="main-body">

                    <!-- author information -->
                    <section class="author-info author-top box mb-20">

                        <img src="uploads/user/user-1.jpg" alt="" class="author-avatar">
                        <div>
                            <h4 class="inline-block font-14 ml-15">نام نویسنده</h4>
                            <p class="inline-block text-muted font-10">
                                <i class="fa fa-pencil-square-o"></i>
                                <span>نوشته ها: 63</span>
                            </p>
                            <p class="text-muted font-11 author-bio">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است</p>
                            <ul class="social-list">
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-telegram"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </section>
                    <!-- /-- author information -->


                    <!-- article list --->
                    <div class="article-list box">
                        <div class="body-">
                            <?php foreach(range(0,4) as $i): ?>
                                <article class="article has-thumb">
                                    <figure>
                                        <a href="../single.blade.php">
                                            <img class="article-img" src="uploads/images/pic-<?= $i%3+1 ?>-768x480.jpg">
                                        </a>
                                        <div class="content">
                                            <h2 class="article-title font-18 mt-0 text-justify text-overflow-ellipsis">
                                                <a href="../single.blade.php">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</a>
                                            </h2>
                                            <p class="text-muted font-11">
                                                <span class="ml-15 inline-block">
                                                    <i class="fa fa-clock-o"></i>
                                                    <span>تاریخ: </span>
                                                    <span>16 اردیبهشت 96</span>
                                                </span>
                                                <span class="ml-15 inline-block">
                                                    <i class="fa fa-bookmark-o"></i>
                                                    <span>دسته بندی: </span>
                                                    <a href="#">هنر</a>
                                                </span>
                                                <span class="inline-block">
                                                    <i class="fa fa-eye"></i>
                                                    <span>بازدید : </span>
                                                    <span>56</span>
                                                </span>
                                            </p>
                                            <p class="short-story font-12 text-muted text-justify mb-0">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                            <a href="../single.blade.php" class="read-more">نمایش بیشتر</a>
                                        </div>
                                    </figure>
                                </article>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <!-- /-- article list --->

                    <!-- pagination --->
                    <div class="text-center mt-30 mb-30">
                        <ul class="pagination">
                            <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        </ul>
                    </div>
                    <!-- /-- pagination --->
                </div>
                <!-- /-- main body --->


            </div>
            <!-- /-- main content --->
            <!-- sidebar --->
            <div class="col-md-4">
                @include('layouts.__sidebar')
            </div><!--col-md-4--->
            <!-- /-- sidebar --->
        </div><!--row--->
    </div><!--container--->
</main>
<!-- /-- main --->

