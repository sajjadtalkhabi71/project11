<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('/home' , 'Admin\HomeController');

//route header
Route::get('/' , 'HomeController@index');
Route::get('/books' , 'BookController@index');
Route::get('/articles' , 'ArticleController@index');
Route::get('/courses' , 'CourseController@index');
Route::get('/episodes' , 'EpisodeController@index');
Route::get('/story' , 'StoryController@index');
Route::get('/games' , 'GameController@index');
Route::post('/comment' , 'HomeController@comment');
Route::post('/discuss' , 'HomeController@discuss');

Route::get('/articles/{articleSlug}' , 'ArticleController@single');
Route::get('/books/{bookSlug}' , 'BookController@single');
Route::get('/courses/{courseSlug}' , 'CourseController@single');
Route::get('/stories/{courseSlug}' , 'StoryController@single');



//register email
//Route::get('/user/active/email/{token}' , 'UserController@activation')->name('activation.account');

//login google
Route::get('login/google', 'Auth\LoginController@redirectToProvider');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');


//namespace('Admin')->prefix('admin')

Route::group(['namespace'=>'Admin' ,'middleware'=>['auth:web'], 'prefix'=>'admin'] ,function(){
    Route::get('/panel','PanelController@index');
    Route::post('/panel/upload-image','PanelController@uploadImageSubject');

    Route::resource('articles', 'ArticleController');
    Route::resource('books', 'BookController');
    Route::resource('courses', 'CourseController');
    Route::resource('stories', 'StoryController');
    Route::resource('games', 'GameController');
    Route::get('/comments/unsuccessful' , 'CommentController@unsuccessful');
    Route::resource('comments', 'CommentController');
    Route::resource('episodes', 'EpisodeController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');


    Route::group(['prefix'=>'users'], function (){
        Route::get('/' , 'UserController@index');
        Route::resource('level' ,'LevelManageController' , ['parameters'=>['level'=>'user '] ]);
        Route::delete('/{user}/destroy' , 'UserController@destroy')->name('users.destroy');
    });
});



Route::group(['namespace' => 'Auth'] , function (){
    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login');
    Route::get('logout', 'LoginController@logout')->name('logout');

    //   login by google
    Route::get('login/google', 'LoginController@redirectToProvider');
    Route::get('login/google/callback', 'LoginController@handleProviderCallback');

    // Registration Routes...
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');
});
