
<header class="page-header-section box info" style="background: linear-gradient(141deg, #FF7800, #e63636 71%, #ff006a);">
    <div class="container">
        <h1>لیست مقالات</h1>
        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با</p>
    </div>
</header>

<!-- main --->
<main id="main">
    <div class="container">
        <div class="row">
            <!-- main content --->
            <div class="col-md-12">
                <!-- main body --->
                <div class="main-body">
                    <!-- article list --->
                    <div class="article-list-2">
                        <div class="row">
                            <?php foreach(range(0,15) as $i): ?>
                                <div class="col-sm-6 col-md-3 col-lg-4 mb-20">
                                    <article class="article clearfix box">
                                        <figure>
                                            <a href="../single.blade.php">
                                                <img class="article-img img-responsive" src="uploads/images/pic-<?= $i%3+1 ?>-768x480.jpg">
                                            </a>
                                            <div class="body">
                                                <p class="text-muted font-11">
                                            <span class="ml-15 inline-block">
                                               <i class="fa fa-user-circle-o"></i>
                                                <span>نویسنده: </span>
                                                <a href="../author.blade.php">کاربر</a>
                                            </span>
                                                    <span class="ml-15 inline-block">
                                                <i class="fa fa-clock-o"></i>
                                                <span>تاریخ: </span>
                                                <span>16 اردیبهشت 96</span>
                                            </span>
                                                    <span class="ml-15 inline-block">
                                                <i class="fa fa-bookmark-o"></i>
                                                <span>دسته بندی: </span>
                                                <a href="#">هنر</a>
                                            </span>
                                                    <span class="inline-block">
                                                <i class="fa fa-eye"></i>
                                                <span>بازدید : </span>
                                                <span>56</span>
                                            </span>
                                                </p>
                                                <hr class="border-top">
                                                <h2 class="article-title font-14 mt-0 text-justify">
                                                    <a href="../single.blade.php" class="text-gray">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با... </a>
                                                </h2>
                                            </div>
                                        </figure>
                                    </article>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <!-- /-- article list --->

                    <!-- pagination --->
                    <div class="text-center mt-30 mb-30">
                        <ul class="pagination">
                            <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        </ul>
                    </div>
                    <!-- /-- pagination --->
                </div>
                <!-- /-- main body --->


            </div>
            <!-- /-- main content --->
        </div><!--row--->
    </div><!--container--->
</main>
<!-- /-- main --->
