<?php

namespace App\Http\Controllers;

use App\Article;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(Article $article)
    {
        $article->increment('viewCount');
        if(cache()->has('articles')) {
            $articles = cache('articles');
        } else {
            $articles = Article::latest()->take(8)->get();
            cache(['articles' => $articles] , Carbon::now()->addMinutes(10));
        }
        return view('articles' , compact('articles'));
    }

    public function single(Article $article)
    {
        $article->increment('viewCount');
        $comments = $article->comments()->where('approved' , 1)
            ->where('parent_id' , 0)->latest()->get();

//        $articles = Article::whereTitle('داستان نویسی')->get();
//        dd($articles);
        return view('Home.articles' , compact('article' ,'comments' ));
    }
}
