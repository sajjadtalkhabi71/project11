<!-- slider -->
<div class="main-slider">
    <?php foreach (range(1,5) as $i): ?>
        <a href="../single" class="slider-item">
            <img src="uploads/slides/book-<?= $i%5+1 ?>.jpg">
            <h3 class="title"></h3>
        </a>
    <?php endforeach; ?>
</div>
<!-- //-- slider -->
<script>
    $(document).ready(function(){
        $('.main-slider').bedunimSlider();
    });
</script>


<!-- main --->
<main id="main">
    <div class="container">
        <div class="row">
            <!-- main content --->
            <div class="col-md-8">

                <!-- main body --->
                <div class="main-body">
                    <!-- article list --->
                    <div class="article-list box">

                        <h3 class="page-title pt-10 pb-10 border-bottom">عنوان لیست</h3>
                        <h3 class="page-title pt-10 pb-10 info bevel border-bottom">عنوان لیست</h3>
                        <h3 class="page-title pt-10 pb-10 warning border-bottom">عنوان لیست</h3>

                        <div class="body-">
                            <?php foreach(range(0,10) as $i): ?>
                                <article class="article has-thumb clearfix">
                                    <figure>
                                        <a href="../single.blade.php">
                                            <img class="article-img" src="uploads/images/pic-<?= $i%3+1 ?>-768x480.jpg" width="225" height="150">
                                        </a>
                                        <div class="content">
                                            <p class="text-muted font-11">
                                                <span class="ml-15 inline-block">
                                                   <i class="fa fa-user-circle-o"></i>
                                                    <span>نویسنده: </span>
                                                    <a href="../author.blade.php">کاربر</a>
                                                </span>
                                                <span class="ml-15 inline-block">
                                                    <i class="fa fa-clock-o"></i>
                                                    <span>تاریخ: </span>
                                                    <span>16 اردیبهشت 96</span>
                                                </span>
                                                <span class="ml-15 inline-block">
                                                    <i class="fa fa-bookmark-o"></i>
                                                    <span>دسته بندی: </span>
                                                    <a href="#">هنر</a>
                                                </span>
                                                <span class="inline-block">
                                                    <i class="fa fa-eye"></i>
                                                    <span>بازدید : </span>
                                                    <span>56</span>
                                                </span>
                                            </p>
                                            <h2 class="article-title font-18 mt-0 text-justify">
                                                <a href="../single.blade.php" class="text-overflow-ellipsis">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و استفاده از طراحان گرافیک است</a>
                                            </h2>
                                            <p class="short-story font-12 text-muted text-justify mb-0">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است</p>
                                            <a href="../single.blade.php" class="read-more">نمایش بیشتر</a>
                                        </div>
                                    </figure>
                                </article>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <!-- /-- article list --->

                    <!-- pagination --->
                    <div class="text-center mt-30 mb-30">
                        <ul class="pagination">
                            <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        </ul>
                    </div>
                    <!-- /-- pagination --->
                </div>
                <!-- /-- main body --->


            </div>
            <!-- /-- main content --->
            <!-- sidebar --->
            <div class="col-md-4">
                @include('layouts.__sidebar')
            </div><!--col-md-4--->
            <!-- /-- sidebar --->
        </div><!--row--->
    </div><!--container--->
</main>
<!-- /-- main --->

<!-- container-fluid --->
<div class="container-fluid">


    <!-- relative articles -->
    <section class="mb-20 mt-20 box">
        <header>
            <i class="fa fa-list fa-flip-horizontal fa-lg ml-10"></i>
            <span>مقالات مرتبط</span>
        </header>
        <div class="body-">
            <div class="full-width-owl owl-carousel owl-theme top-left-nav">
                <?php foreach (range(1, 12) as $i): ?>
                    <div class="item">
                        <a href="#">
                            <img src="uploads/images/pic-<?= $i%3+1 ?>-768x480.jpg" alt="">
                        </a>
                        <p class="text-muted font-11 mb-0 body">
                                <span class="ml-15 inline-block">
                                   <i class="fa fa-user-circle-o"></i>
                                    <a href="../author.blade.php">کاربر</a>
                                </span>
                            <span class="ml-15 inline-block">
                                    <i class="fa fa-clock-o"></i>
                                    <span>16 اردیبهشت 96</span>
                                </span>
                            <span class="ml-15 inline-block">
                                    <i class="fa fa-bookmark-o"></i>
                                    <a href="#">هنر</a>
                                </span>
                            <span class="inline-block">
                                    <i class="fa fa-eye"></i>
                                    <span>56</span>
                                </span>
                        </p>
                        <div class="body border-top body">
                            <h3 class="text-center font-13 mt-0">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با</h3>
                        </div>

                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </section>
    <!-- /-- relative articles --->

    <!-- article list --->
    <section class="article-cards">
        <h3 class="border-bottom border-bottom-primary-- text-primary-- text-center pt-20 pb-5">عنوان لیست</h3>
        <div class="full-width-owl owl-carousel owl-theme top-nav">
            <?php foreach(range(0,15) as $i): ?>
                <div class="item">
                    <article class="article clearfix box">
                        <figure>
                            <a href="../single.blade.php">
                                <img class="article-img img-responsive" src="uploads/images/pic-<?= $i%3+1 ?>-768x480.jpg">
                            </a>
                            <div class="body">
                                <h2 class="article-title font-14 mt-0 text-justify">
                                    <a href="../single.blade.php" class="text-gray">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با... </a>
                                </h2>
                                <p class="text-justify text-muted font-11">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی</p>
                                <hr class="border-top">
                                <p class="text-muted font-11 mb-0">
                                    <span class="ml-15 inline-block">
                                       <i class="fa fa-user-circle-o"></i>
                                        <a href="../author.blade.php">کاربر</a>
                                    </span>
                                    <span class="ml-15 inline-block">
                                        <i class="fa fa-clock-o"></i>
                                        <span>16 اردیبهشت 96</span>
                                    </span>
                                    <span class="ml-15 inline-block">
                                        <i class="fa fa-bookmark-o"></i>
                                        <a href="#">هنر</a>
                                    </span>
                                    <span class="inline-block">
                                        <i class="fa fa-eye"></i>
                                        <span>56</span>
                                    </span>
                                </p>
                            </div>
                        </figure>
                    </article>
                </div>
            <?php endforeach ?>
        </div>
    </section>
    <!-- /-- article list --->

</div>
<!-- /-- container-fluid --->
