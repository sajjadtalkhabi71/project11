<!-- main --->
<main id="main">
    <div class="container">
        <div class="row">
            <!-- main content --->
            <div class="col-md-8">


                <h3 class="page-title">نتایج جستجو</h3>

                <!-- main body --->
                <div class="main-body">
                    <!-- article list --->
                    <div class="article-list box">
                        <div class="body">

                            <section class="search-form-large mb-20 border-bottom">
                                <form action="" method="get">

                                    <div class="row">
                                        <div class="col-sm-12 col-sm-pull-1-">
                                            <div class="row">
                                                <div class="col-sm-9 mb-20">
                                                    <input type="search" name="q" placeholder="دنبال چی میگردی ؟" class="form-control input-lg" required>
                                                </div>
                                                <div class="col-sm-3 mb-20">
                                                    <button type="submit" class="btn btn-block btn-primary btn-lg"><span>جستجو</span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </section>

                            <?php foreach(range(0,4) as $i): ?>
                                <article class="article">
                                    <figure>
                                        <div class="content">
                                            <h2 class="article-title font-15 mt-0 text-justify">
                                                <a href="../single.blade.php"><i class="fa fa-angle-double-left ml-10 text-primary"></i>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و ...</a>
                                            </h2>
                                            <p class="text-muted font-11 mb-0 pr-20">
                                    <span class="ml-15 inline-block">
                                       <i class="fa fa-user-circle-o"></i>
                                        <span>نویسنده: </span>
                                        <a href="../author.blade.php">کاربر</a>
                                    </span>
                                                <span class="ml-15 inline-block">
                                        <i class="fa fa-clock-o"></i>
                                        <span>تاریخ: </span>
                                        <span>16 اردیبهشت 96</span>
                                    </span>
                                                <span class="ml-15 inline-block">
                                        <i class="fa fa-bookmark-o"></i>
                                        <span>دسته بندی: </span>
                                        <a href="#">هنر</a>
                                    </span>
                                                <span class="inline-block">
                                        <i class="fa fa-eye"></i>
                                        <span>بازدید : </span>
                                        <span>56</span>
                                    </span>
                                            </p>
                                        </div>
                                    </figure>
                                </article>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <!-- /-- article list --->

                    <!-- pagination --->
                    <div class="text-center mt-30 mb-30">
                        <ul class="pagination">
                            <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        </ul>
                    </div>
                    <!-- /-- pagination --->
                </div>
                <!-- /-- main body --->


            </div>
            <!-- /-- main content --->
            <!-- sidebar --->
            <div class="col-md-4">
                <?php include '__sidebar.blade.php' ?>
            </div><!--col-md-4--->
            <!-- /-- sidebar --->
        </div><!--row--->
    </div><!--container--->
</main>
<!-- /-- main --->
