
<div class="sidebar">
    <div class="row">
        <div class="col-sm-6 col-md-12 mb-20">
            <!-- last news --->
            <section class="box last-news">
                <header>
                    <i class="fa fa-lg fa-newspaper-o ml-10"></i>
                    <span>آخرین اخبار</span>
                </header>
                <div class="body-">
                    <?php foreach (range(0,4) as $i): ?>
                        <article class="article">
                            <a href="../single.blade.php" title="">
                                <figure class="clearfix">
                                    <div class="article-img video-overlay">
                                        <img src="uploads/images/pic-<?= $i%3+1 ?>-150x150.jpg" alt="" class="">
                                    </div>
                                    <div class="content">
                                        <h4 class="font-13 mt-0 pt-10 article-title text-overflow-ellipsis" title="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</h4>
                                        <p class="font-10 text-muted mb-0">
                                            <i class="fa fa-clock-o"></i> <span>16 اردیبهشت 96</span>
                                        </p>
                                    </div>
                                </figure>
                            </a>
                        </article>
                    <?php endforeach ?>
                </div>
            </section>
            <!-- /-- last news --->

            <!-- simple-list --->
            <section class="box sidebar-list mt-20">
                <header>
                    <i class="fa fa-lg fa-newspaper-o ml-10"></i>
                    <span>لیست ساده</span>
                </header>
                <div class="body">
                    <?php foreach (range(0,4) as $i): ?>
                        <article class="article mb-15">
                            <a href="../single.blade.php" class="text-gray" title="">
                                <h4 class="font-13 mt-0 pt-10 article-title text-overflow-ellipsis">
                                    <i class="fa fa-angle-double-left ml-5"></i>   لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
                                </h4>
                                <p class="font-10 text-muted mb-0">
                                    <i class="fa fa-clock-o"></i> <span>16 اردیبهشت 96</span>
                                </p>
                            </a>
                        </article>
                    <?php endforeach ?>
                </div>
            </section>
            <!-- /-- simple-list --->

            <!-- simple-list-icon --->
            <section class="box sidebar-list mt-20">
                <header>
                    <i class="fa fa-lg fa-newspaper-o ml-10"></i>
                    <span>لیست با آیکن</span>
                </header>
                <div class="body-">
                    <?php foreach (range(0,4) as $i): ?>
                        <article class="article clearfix border-bottom">
                            <a href="../single.blade.php" class="text-gray hover-<?= @['primary', 'info', 'success', 'warning', 'danger'][$i] ?>" title="">
                                <div class="article-icon ml-15 <?= @['primary', 'info', 'success', 'warning', 'danger'][$i] ?>">
                                    <i class="fa fa-<?= @['chevron-circle-left', 'commenting-o', 'heartbeat', 'star', 'recycle'][$i] ?> angle-double-left"></i>
                                </div>
                                <div class="body">
                                    <h4 class="font-13 mt-0 article-title text-overflow-ellipsis">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ</h4>
                                    <p class="font-10 text-muted mb-0">
                                        <i class="fa fa-clock-o"></i> <span>16 اردیبهشت 96</span>
                                    </p>
                                </div>
                            </a>
                        </article>
                    <?php endforeach ?>
                </div>
            </section>
            <!-- /-- simple-list-icon --->

        </div>
        <div class="col-sm-6 col-md-12">

            <!-- newsletter --->
            <section class="box sidebar-newsletter">
                <header>
                    <i class="fa fa-lg fa-envelope-o ml-10"></i>
                    <span>خبرنامه</span>
                </header>
                <div class="body">
                    <form action="___ajax_newsletter.blade.php" method="post" id="newsletter-form" class="ajax-submit" novalidate>
                        <p class="text-center text-muted font-15"> ثبت نام در خبرنامه </p>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="email" name="email" class="form-control" title="" placeholder="ایمیل خود را وارد کنید" required>
                                <span class="input-group-addon bg-white">
                                    <i class="fa fa-envelope-o"></i>
                                </span>
                            </div>
                        </div>
                        <div class="ajax-result pull-right mt-10"></div>
                        <div class="text-left">
                            <button type="submit" class="btn btn-primary">عضو میشم</button>
                        </div>
                    </form>
                    <hr>
                    <p class="text-center text-muted font-10">برای دریافت مطالب جذاب روزانه، عضو خبرنامه شوید</p>
                </div>
            </section>
            <!-- /-- newsletter --->

            <!-- simple-list-2 --->
            <section class="box sidebar-list-img mt-20">
                <header>
                    <i class="fa fa-lg fa-newspaper-o ml-10"></i>
                    <span>لیست با تصویر</span>
                </header>
                <div class="body-">
                    <?php foreach (range(0,2) as $i): ?>
                        <article class="article">
                            <a href="../single.blade.php" title="" class="text-gray">
                                <figure class="clearfix title-over">
                                    <img src="uploads/images/pic-<?= $i%3+1 ?>-768x480.jpg" alt="" class="article-img img-responsive">
                                    <h4 class="font-13 mt-0 pt-15 pb-15 article-title">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</h4>
                                </figure>
                            </a>
                        </article>
                    <?php endforeach ?>
                </div>
            </section>
            <!-- /-- simple-list --->

            <!-- banners --->
            <aside class="mt-20">
                <a href="#" title=""><img src="/images/ads-1.jpg" class="mb-20 img-responsive" alt=""></a>
                <a href="#" title=""><img src="/images/ads-2.jpg" class="mb-20 img-responsive" alt=""></a>
            </aside>
            <!-- /-- banners --->

        </div>
    </div><!--row--->
</div><!--sidebar--->
