<!-- main --->
<main id="main">
    <div class="container">
        <div class="row">
            <!-- main content --->
            <div class="col-md-8">

                <!-- main body --->
                <div class="main-body">
                    <!-- article list --->
                    <div class="article-single box mb-20">

                        <article class="article">

                            <h1 class="mb-0 border-bottom font-16 page-title pt-10 pb-10 bevel danger">
                                <a href="#" title="" class="text-muted hover-warning">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</a>
                            </h1>

                            <figure>
                                <div class="details pl-15 pr-15 pt-20 border-top-">
                                    <p class="text-muted font-11 mb-20">
                                        <span class="ml-15 inline-block">
                                           <i class="fa fa-user-circle-o"></i>
                                            <span>نویسنده: </span>
                                            <a href="../author.blade.php">کاربر</a>
                                        </span>
                                        <span class="ml-15 inline-block">
                                            <i class="fa fa-clock-o"></i>
                                            <span>تاریخ: </span>
                                            <span>16 اردیبهشت 96</span>
                                        </span>
                                        <span class="ml-15 inline-block">
                                            <i class="fa fa-bookmark-o"></i>
                                            <span>دسته بندی: </span>
                                            <a href="#">هنر</a>
                                        </span>
                                        <span class="inline-block">
                                            <i class="fa fa-eye"></i>
                                            <span>بازدید : </span>
                                            <span>56</span>
                                        </span>
                                    </p>
                                </div>
                                <a href="#">
                                    <img class="mb-10 article-img img-responsive" src="uploads/images/pic-1-768x480.jpg">
                                </a>
                            </figure>
                            <div class="body pt-0">
                                <div class="full-story font-14 text-justify mb-30">
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                    <h4>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</h4>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                    <h4>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</h4>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                </div>

                                <div class="tags text-muted">
                                    <i class="fa fa-tag fa-flip-horizontal"></i>
                                    <a href="#">برچسب</a>
                                    <a href="#">برچسب</a>
                                    <a href="#">برچسب</a>
                                    <a href="#">برچسب</a>
                                    <a href="#">برچسب</a>
                                </div>
                            </div>

                        </article>

                    </div>
                    <!-- /-- article .box --->

                    <!-- next-prev article -->
                    <section class="box next-prev-article">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="../single.blade.php" title="">
                                    <img src="uploads/images/pic-1-150x150.jpg" alt="">
                                    <p>نوشته قبلی</p>
                                    <h3>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ</h3>
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="../single.blade.php" title="">
                                    <img src="uploads/images/pic-2-150x150.jpg" alt="">
                                    <p>نوشته بعدی</p>
                                    <h3>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ</h3>
                                </a>
                            </div>
                        </div>
                    </section>
                    <!-- /-- next-prev article --->

                    <!-- author information -->
                    <section class="author-info author-top box mb-20 mt-20">
                        <img src="uploads/user/user-1.jpg" alt="" class="author-avatar">
                        <div>
                            <h4 class="inline-block font-14 ml-15"><span>نویسنده: </span> <a href="../author.blade.php">نام نویسنده</a></h4>
                            <p class="text-muted font-11 author-bio">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است</p>
                            <ul class="social-list">
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-telegram"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </section>
                    <!-- /-- author information -->

                    <!-- relative articles -->
                    <section class="mb-20 box">
                        <header>
                            <i class="fa fa-list fa-flip-horizontal fa-lg ml-10"></i>
                            <span>مقالات مرتبط</span>
                        </header>
                        <div class="body-">
                            <div class="relative-articles owl-carousel owl-theme top-left-nav">
                                <a href="#" class="item">
                                    <img src="uploads/images/pic-1-768x480.jpg" alt="">
                                    <p>فرهنگی - چهارشنبه 23 مرداد</p>
                                    <h3>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با</h3>
                                </a>
                                <a href="#" class="item">
                                    <img src="uploads/images/pic-2-768x480.jpg" alt="">
                                    <p>فرهنگی - چهارشنبه 23 مرداد</p>
                                    <h3>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با</h3>
                                </a>
                                <a href="#" class="item">
                                    <img src="uploads/images/pic-3-768x480.jpg" alt="">
                                    <p>فرهنگی - چهارشنبه 23 مرداد</p>
                                    <h3>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با</h3>
                                </a>
                                <a href="#" class="item">
                                    <img src="uploads/images/pic-2-768x480.jpg" alt="">
                                    <p>فرهنگی - چهارشنبه 23 مرداد</p>
                                    <h3>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با</h3>
                                </a>
                            </div>
                        </div>
                    </section>
                    <!-- /-- relative articles --->


                    <!-- comments -->
                    <section class="box mb-30">

                        <header>
                            <i class="fa fa-commenting-o fa-lg ml-10"></i>
                            <span>نظرات</span> <span>(4)</span>
                        </header>

                        <div class="body">

                            <!-- comment form -->
                            <form class="row comment-form ajax-submit" action="___ajax_comment.blade.php" method="post" novalidate>
                                <div class="col-sm-6 col-md-7 col-sm-pull-6 col-md-pull-5">
                                    <div class="form-group">
                                        <textarea class="form-control" name="message" placeholder="نظر ..." required></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-sm-push-6 col-md-push-7">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" placeholder="نام" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control" placeholder="ایمیل" required>
                                    </div>
                                    <button type="submit" class="btn btn-block btn-warning"><i class="fa  fa-paper-plane-o ml-15"></i><span>ارسال</span></button>
                                </div>
                                <div class="col-xs-12 text-center ajax-result"></div>
                            </form>
                            <!-- /-- comment form --->

                            <div class="divider border-bottom mt-20 mb-20"></div>

                            <!-- comments list -->
                            <div class="comments">

                                <?php foreach (range(0,10) as $i): ?>
                                    <div class="comment-item">
                                        <h4 class="font-12 mt-0">
                                            <span class="ml-15 text-primary">نویسنده نظر</span>
                                            <span class="text-muted font-10"><i class="fa fa-clock-o"></i> <span>23 بهمن 95</span></span>
                                        </h4>
                                        <p class="text-muted mt-10 mb-0 font-12 text-justify">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                    </div>
                                <?php endforeach; ?>

                            </div>
                            <!-- /-- comments list --->

                        </div><!--.body--->

                    </section>
                    <!-- /-- comments --->

                </div>
                <!-- /-- main body --->

            </div>
            <!-- /-- main content --->
            <!-- sidebar --->
            <div class="col-md-4">
                @include('layouts.__sidebar')
            </div><!--col-md-4--->
            <!-- /-- sidebar --->
        </div><!--row--->
    </div><!--container--->
</main>
<!-- /-- main --->
