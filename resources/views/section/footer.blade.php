<!-- footer --->
<footer id="footer" class="mt-30">
    <hr class="mb-60 mt-0"/>

    <div class="container pt-10">
        <div class="row">

            <section class="col-sm-6 col-md-3 mb-30">
                <div class="short-about">
                    <!--<h4 class="section-title">درباره ما</h4>-->
                    <div class="text-center">
                        <img src="images/logo-1.png" alt="" class="footer-logo">
                    </div>
                </div>
            </section>

            <section class="col-sm-6 col-md-3 mb-30">
                <div class="contact-us-info pb-10">
                    <h4 class="section-title">تماس با ما</h4>
                    <p>
                        <i class="fa fa-lg fa-phone-square"></i>
                        <span class="en">00-1111-2222</span>
                    </p>
                    <p>
                        <i class="fa fa-lg fa-envelope-o"></i>
                        <span class="en">info@site.com</span>
                    </p>
                    <p>
                        <i class="fa fa-lg fa-map-marker"></i>
                        <span>قم - پلاک 1</span>
                    </p>
                </div>
                <div class="pt-30">
                    <h4 class="section-title">ما را در شبکه های اجتماعی دنبال کنید</h4>
                    <ul class="social-list">
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fa fa-telegram"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
            </section>

            <section class="col-sm-4 col-md-3 mb-30">
                <h4 class="section-title">لینک های مفید</h4>
                <ul class="link-list">
                    <li><a href="#">لینک اول</a></li>
                    <li><a href="#">لینک دوم</a></li>
                    <li><a href="#">لینک سوم</a></li>
                    <li><a href="#">لینک چهارم</a></li>
                    <li><a href="#">لینک پنجم</a></li>
                </ul>
            </section>

            <section class="col-sm-4 col-md-3 mb-30">
                <h4 class="section-title">لینک های دیگر</h4>
                <ul class="link-list">
                    <li><a href="#">لینک اول</a></li>
                    <li><a href="#">لینک دوم</a></li>
                    <li><a href="#">لینک سوم</a></li>
                    <li><a href="#">لینک چهارم</a></li>
                    <li><a href="#">لینک پنجم</a></li>
                </ul>
            </section>

        </div>
    </div>

    <!-- copyright --->
    <hr class="mt-60 mb-10"/>
    <div class="copyright">
        <div class="container text-center">
            <span class="font-11">تمام حقوق مادی و معنوی محفوظ است</span>
        </div>
    </div>
    <hr class="mb-0 mt-10"/>
    <!-- /-- copyright --->

</footer>
<!-- /-- footer --->

</body>
</html>
