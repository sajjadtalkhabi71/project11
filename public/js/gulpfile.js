var gulp = require('gulp');
var header = require('gulp-header');
var sass = require('gulp-sass');
var watch = require('gulp-watch');




var pkg = require('./package.json');
var banner = ['/**',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version v<%= pkg.version %>',
  ' * @link <%= pkg.homepage %>',
  ' * @license <%= pkg.license %>',
  ' */',
  ''].join('\n');

/*
gulp.src('css/*.css')
  .pipe(header(banner, { pkg : pkg } ))
  .pipe(gulp.dest('css/'));

gulp.src('js/*.js')
  .pipe(header(banner, { pkg : pkg } ))
  .pipe(gulp.dest('js/'));
*/


gulp.task('sass', function() {
    gulp.src('assets/sass/**/*.scss')
        .pipe(sass({
            outputStyle: 'compact' // Default: nested.   Values: nested, expanded, compact, compressed
        }).on('error', sass.logError))
        .pipe(gulp.dest('./assets/css/'))
});

//Watch task
gulp.task('watch', function () {
	gulp.watch('assets/sass/**/*.scss', ['sass']);
});


gulp.task('default', ['sass']);