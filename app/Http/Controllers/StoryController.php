<?php

namespace App\Http\Controllers;

use App\Story;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StoryController extends Controller
{
    public function index(Story $story)
    {
        $story->increment('viewCount');
        if(cache()->has('storys')) {
            $storys = cache('storys');
        } else {
            $storys = Story::latest()->take(8)->get();
            cache(['storys' => $storys] , Carbon::now()->addMinutes(10));
        }
        return view('stories' , compact('storys'));
    }

    public function single(Story $story)
    {
        $story->increment('viewCount');
        $comments = $story->comments()->where('approved' , 1)
            ->where('parent_id' , 0)->latest()->get();

//        $storys = Story::whereTitle('داستان نویسی')->get();
//        dd($storys);
        return view('Home.articles' , compact('story' ,'comments' ));
    }
}
