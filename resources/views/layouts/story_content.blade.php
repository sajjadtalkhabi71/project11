

<header class="page-header-section box info" style="background: linear-gradient(141deg, #FF7800, #ffc500 71%, #FF7800);">
    <div class="container">
        <h1>لیست داستان ها</h1>
        <p>با خواندن داستان های مورد علاقه تان گامی برای دانش اندوزی خود بردارید</p>
    </div>
</header>

<!-- main --->
<main id="main">
    <div class="container">
        <div class="row">
            <!-- main content --->
            <div class="col-md-12">

                <!-- main body --->
                <div class="main-body">
                    <!-- article list --->
                    <div class="article-list box">
                        <div class="body-">
                            @foreach($storys as $story)
                                <div class="col-sm-4 col-lg-4 col-md-4">
                                    <div class="thumbnail">
                                        <img src="{{ $story->images['thumb'] }}" alt="">
                                        <div class="caption">
                                            <h4><a href="{{ $story->path() }}">{{ $story->title }}</a>
                                            </h4>
                                            <p>{{ \Illuminate\Support\Str::limit($story->description , 120) }}</p>
                                        </div>
                                        <div class="ratings">
                                            <p class="pull-right">{{ $story->viewCount }} بازدید</p>
{{--                                            <p class="pull-right">{{ Redis::get("views.{$story->id}.articles") }} بازدید</p>--}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach

{{--                            <?php foreach(range(0,10) as $i): ?>--}}
{{--                                <article class="article has-thumb clearfix">--}}
{{--                                    <figure>--}}
{{--                                        <a href="../single.blade.php">--}}
{{--                                            <img class="article-img" src="uploads/images/pic-<?= $i%3+1 ?>-768x480.jpg">--}}
{{--                                        </a>--}}
{{--                                        <div class="content">--}}
{{--                                            <h2 class="article-title font-18 mt-0 text-justify">--}}
{{--                                                <a href="../single.blade.php">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با... </a>--}}
{{--                                            </h2>--}}
{{--                                            <p class="text-muted font-11">--}}
{{--                                    <span class="ml-15 inline-block">--}}
{{--                                       <i class="fa fa-user-circle-o"></i>--}}
{{--                                        <span>نویسنده: </span>--}}
{{--                                        <a href="../author.blade.php">کاربر</a>--}}
{{--                                    </span>--}}
{{--                                                <span class="ml-15 inline-block">--}}
{{--                                        <i class="fa fa-clock-o"></i>--}}
{{--                                        <span>تاریخ: </span>--}}
{{--                                        <span>16 اردیبهشت 96</span>--}}
{{--                                    </span>--}}
{{--                                                <span class="ml-15 inline-block">--}}
{{--                                        <i class="fa fa-bookmark-o"></i>--}}
{{--                                        <span>دسته بندی: </span>--}}
{{--                                        <a href="#">هنر</a>--}}
{{--                                    </span>--}}
{{--                                                <span class="inline-block">--}}
{{--                                        <i class="fa fa-eye"></i>--}}
{{--                                        <span>بازدید : </span>--}}
{{--                                        <span>56</span>--}}
{{--                                    </span>--}}
{{--                                            </p>--}}
{{--                                            <p class="short-story font-12 text-muted text-justify mb-0">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>--}}
{{--                                        </div>--}}
{{--                                    </figure>--}}
{{--                                </article>--}}
{{--                            <?php endforeach ?>--}}
                        </div>
                    </div>
                    <!-- /-- article list --->

                    <!-- pagination --->
                    <div class="text-center mt-30 mb-30">
                        <ul class="pagination">
                            <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                        </ul>
                    </div>
                    <!-- /-- pagination --->
                </div>
                <!-- /-- main body --->


            </div>
            <!-- /-- main content --->
        </div><!--row--->
    </div><!--container--->
</main>
<!-- /-- main --->

