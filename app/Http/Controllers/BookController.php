<?php

namespace App\Http\Controllers;


use App\Book;
use Carbon\Carbon;


class BookController extends Controller
{
    public function index(book $book)
    {
        $book->increment('viewCount');
        if(cache()->has('books')) {
            $books = cache('books');
        } else {
            $books = book::latest()->take(8)->get();
            cache(['books' => $books] , Carbon::now()->addMinutes(10));
        }
        return view('books' , compact('books'));
    }

    public function single(book $book)
    {
        $book->increment('viewCount');
        $comments = $book->comments()->where('approved' , 1)
            ->where('parent_id' , 0)->latest()->get();

//        $books = book::whereTitle('داستان نویسی')->get();
//        dd($books);
        return view('Home.books' , compact('book' ,'comments' ));
    }
}
