<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>وبسایت علمی پژوهشی خیام</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=yes"/>
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css?v=<?= time() ?>"/>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/theme.js?v=<?= time() ?>"></script>
</head>
<body>


<!-- header --->
<header id="header">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <a href="#"><img src="images/logo-1.png" class="site-logo pt-10 pb-10"></a>
                </div>

                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-md-8 header-col">

                            <!-- search --->
                            <form action="../search.blade.php" method="get" class="search-form">
                                <div class="input-group input-group-lg">
                                    <input type="search" class="form-control font-12" name="q" placeholder="دنبال چی میگردی؟" required="required"/>
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search text-muted"></i></button>
                                    </div>
                                </div>
                            </form>
                            <!-- /-- search --->
                        </div>
                        <div class="col-md-4 header-col">
                            <div class="btn-group-">
                                <a href="/login" class="btn btn-default btn-lg">
                                    <i class="fa fa-sign-in fa-flip-horizontal"></i>
                                    <span class="mr-5 font-12 vm">ورود</span>
                                </a>
                                <a href="/register" class="btn btn-default btn-lg">
                                    <i class="fa fa-user-plus"></i>
                                    <span class="mr-5 font-12 vm">ثبت نام</span>
                                </a>
{{--                                <a href="/admin.panel" class="btn btn-default btn-lg">--}}
{{--                                    <i class="fa fa-sign-in fa-flip-horizontal"></i>--}}
{{--                                    <span class="mr-5 font-12 vm">پنل اصلی</span>--}}
{{--                                </a>--}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="main-nav">
        <div class="container">
            <div class="row">
                <!-- navigation --->
                <div class="col-sm-12">
                    <div class="visible-xs toggle-nav-wrapper">
                        <span class="toggle-nav" onclick="$(this).parents('.main-nav').add(this).toggleClass('nav-active open')"></span>
                    </div>
                    <ul class="main-nav-ul">
                        <li class="active"><a href="/home">صفحه اصلی</a></li>
                        <li><a href="/books">کتاب ها</a></li>
                        <li><a href="/articles">مقالات</a></li>
                        <li><a href="/courses">دوره های آموزشی</a></li>
                        <li>
                            <a href="#">داستان ها</a>
                            <ul>
                                <li>
                                    <a href="/admin/stories">داستان نویسی</a>
                                </li>
                                <li>
                                    <a href="/story">داستان ها</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="/games">بازی و سرگرمی</a></li>
                        <li><a href="/discuss">بحث و گفتگوها</a></li>
                        <li>
                            <a href="#">فروشگاه</a>
                            <ul>
                                <li>
                                    <a href="#">پرفروش ترین</a>
                                    <ul>
                                        <li><a href="#">کالای دیجیتال</a></li>
                                        <li><a href="#">لوازم آرایشی</a></li>
                                        <li><a href="#">مد و لباس</a></li>
                                        <li><a href="#">سطح سه</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">سوپرمارکت</a>
                                    <ul>
                                        <li><a href="#">غذا و آشامیدنی</a></li>
                                        <li><a href="#">خشکبار</a></li>
                                        <li><a href="#">سطح سه</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="../list_2.blade.php">ارتباط با ما</a></li>
                        <li><a href="/admin/panel">پنل اصلی</a></li>
                    </ul>
                </div>
                <!-- /-- navigation --->
            </div>
        </div>
    </nav>
</header>
<!-- /-- header --->
