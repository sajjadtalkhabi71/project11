<?php

namespace App\Http\Controllers;

use App\Article;
use App\Course;
use Carbon\Carbon;



class CourseController extends Controller
{
    public function index(Course $course)
    {
        $course->increment('viewCount');
        if(cache()->has('courses')) {
            $courses = cache('courses');
        } else {
            $courses = Course::latest()->take(8)->get();
            cache(['courses' => $courses] , Carbon::now()->addMinutes(10));
        }
        return view('courses' , compact('courses'));
    }

    public function single(Course $course)
    {
        $course->increment('viewCount');
        $comments = $course->comments()->where('approved' , 1)
            ->where('parent_id' , 0)->latest()->get();

//        $courses = Course::whereTitle('داستان نویسی')->get();
//        dd($courses);
        return view('Home.courses' , compact('course' ,'comments' ));
    }
}
