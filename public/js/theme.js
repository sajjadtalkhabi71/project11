$(document).ready(function(){

    $('.main-nav-ul ul').each(function(i, el){
        var $el = $(el),
            $li = $el.parent();

        $li.addClass('has-sub-menu');
        $('<span/>', {'class': 'toggle-sub-menu'})
            .append('<i class="fa fa-angle-double-down"></i>')
            .appendTo($li)
            .on('click', function(){
                //$('.main-nav-ul ul li').not($li).removeClass('open');
                $li.toggleClass('open');
            });
    });

    $('.main-nav-ul li.has-sub-menu > a[href="#"]').click(function(e){
        e.preventDefault();
        console.log('11520');
        $(this).next().next().trigger('click');
    });

    $('.relative-articles').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        dots: true,
        rtl: true,
        navText: ['<i class="fa fa-angle-right"></i>', '<i class="fa fa-angle-left"></i>'],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });

    $('.full-width-owl').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        dots: true,
        rtl: true,
        navText: ['<i class="fa fa-angle-right"></i>', '<i class="fa fa-angle-left"></i>'],
        responsive: {
            0:{
                items:1
            },
            450:{
                items:2
            },
            600:{
                items:3
            },
            750:{
                items:4
            },
            900:{
                items:5
            },
            1200:{
                items:6
            }
        }
    });

    $(document).on('submit', '.ajax-submit', function(e){
        e.preventDefault();
        var $form = $(this);
        var $btn = $form.find('[type="submit"]');
        var $result = $form.find('.ajax-result');
        var $req = $form.find('[required]');
        var stop = false;

        $req.each(function(i, el){
            var $el = $(el), val = $.trim($el.val());
            if(val == '') {
                stop = true;
                $el.focus();
                return false;
            }
        });

        if(stop) return false;

        $btn.addClass('loading loading-on-corner').prop('disabled', true);

        $result.empty();

        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: new FormData($form[0]),
            dataType: 'json',
            processData: false,
            cache: false,
            success: function(data){
                $result.html('<span class="success text-success">' + data.response + '</span>');
                $btn.removeClass('loading loading-on-corner').prop('disabled', false);
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR, textStatus, errorThrown);
                var text = jqXHR.responseText;
                //if(false){  text = 'خطا در اتصال'; }
                $result.html('<span class="error text-danger">' + text + '</span>');
                $btn.removeClass('loading loading-on-corner').prop('disabled', false);
            }
        });
    });

    $('[title]').tooltip();
});


/**
 * Bedunim jQuery slider
 * version 1.0
 * @returns {*}
 */
$.fn.bedunimSlider = function(){
    return this.each(function () {

        var slider = {
            $el: $(this),
            $items: $(this).find('.slider-item'),
            indexes: []
        };

        slider.$items.each(function (i, el) {
            el = $(el);
            el.css({
                'background-image': 'url("' + el.find('img').attr('src') +  '")'
            });
            slider.indexes.push(i);
            var $i = $('<i/>', {'class': 'fa fa-play-circle-o go-to-item'}).appendTo(el);
            $i.on('click', function(e){
                e.preventDefault();
                slider.goTo(el);
            });
            el.on('mouseover', function(){
                el.addClass('hover');
            }).on('mouseleave', function(){
                el.removeClass('hover');
            })
        });

        slider.play = function(){
            this.$items.each(function(i, el){
                el = $(el);
                var index = slider.indexes[i];
                el.attr('data-index', index);

                if(index > 4){
                    el.addClass('sidebar-item-hidden');
                }else{
                    el.removeClass('sidebar-item-hidden');
                }
                el.removeClass('hover');
            });
            this.indexes.unshift(this.indexes.pop());
            clearTimeout(this.timeOut);
            this.timeOut = setTimeout(function(){
                slider.play();
            }, 8000);

        };

        slider.goTo = function($el){
            var c = $el.index();
            while(this.indexes[c] != 0){
                this.indexes.unshift(this.indexes.pop());
            }
            slider.play();
        };

        slider.play();
        setTimeout(function () {
            slider.$el.addClass('loaded');
        }, 500);
    });
};